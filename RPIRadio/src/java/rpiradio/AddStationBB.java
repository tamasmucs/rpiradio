/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rpiradio;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class AddStationBB {
    @Inject
    StationManager manager;
    
    private String station;

    public void setStation(String station) {
        this.station = station;
    }

    public String getStation() {
        return station;
    }
    
    public String addStation(){
        manager.getStations().add(station);
        return "index?faces-redirect=true";
    }
}
