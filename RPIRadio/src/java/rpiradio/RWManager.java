/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rpiradio;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author tamas
 */
public class RWManager {

    private static final String FILEPATH = "/var/lib/radiod/stationlist";

    public static boolean writeToFile(List<String> stations){
        boolean result = false;
        
        try{
            FileWriter fw = new FileWriter(FILEPATH,false);
            if(stations == null) stations= new LinkedList<>();
            for (String station : stations){
                System.out.println("Writing: "+ station);
                fw.append(station+System.lineSeparator());
            }
            
            result = true;
            fw.close();
        }catch(IOException e){
            System.out.println("Failed to write to file -> IOException");
        }
        return result;
    }
    
    public static List<String> readStationFile(){
        List<String> result = new LinkedList<>();
        
        try {
            BufferedReader br = new BufferedReader(new FileReader(FILEPATH));
            String line = br.readLine();

            while (line != null) {
                result.add(line);
                line = br.readLine();
            }
            br.close();
        } catch(IOException e){
            result = null;
        }finally {
            
        }

        return result;
    }
}
