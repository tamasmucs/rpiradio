/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rpiradio;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class RemoveStationBB {
    @Inject
    StationManager manager;
    
    public String removeStation(String station){
        manager.getStations().remove(station);
        return "index?faces-redirect=true";
    }
}
