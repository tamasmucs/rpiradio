/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rpiradio;

import java.util.LinkedList;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

@Named
@ApplicationScoped
public class StationManager {
    private List<String> stations= new LinkedList<>();

    public StationManager() {
        readFromFile();
    }
    
    public void setStations(List<String> stations) {
        this.stations = stations;
    }

    public List<String> getStations() {
        return stations;
    }
    
    public void readFromFile(){
        this.stations = RWManager.readStationFile();
    }
    
   
}
