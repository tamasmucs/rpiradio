/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rpiradio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author tmx
 */
public class TerminalManager {
    public static void UpdatePlaylist() throws IOException, InterruptedException{
        TerminalManager.ExecCommand("sudo /home/pi/radio/create_playlists.py --delete_old");
    }
    
    public static void RestartRadiod() throws IOException, InterruptedException{
        TerminalManager.ExecCommand("sudo service radiod start");
    }
    
    private static void ExecCommand(String cmd) throws IOException, InterruptedException{
        Process p = Runtime.getRuntime().exec(cmd);
        p.waitFor();
        
        String line = "";
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        while ((line = reader.readLine()) != null) {
            System.out.println(line); //To Server Log
        }
    }
}
