/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rpiradio;

import java.io.IOException;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@RequestScoped
public class WriteToFileBB {
    @Inject
    StationManager manager;
    
     public String writeToFile() throws IOException, InterruptedException{
        if(RWManager.writeToFile(manager.getStations())){
            TerminalManager.UpdatePlaylist();
            TerminalManager.RestartRadiod();
            return "index?faces-redirect=true";
        }else{
            return "RWError";
        }
    }
}
